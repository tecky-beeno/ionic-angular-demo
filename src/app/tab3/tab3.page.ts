import { Component } from '@angular/core'

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
})
export class Tab3Page {
  items: string[] = []

  newText = ''

  constructor() {}

  addItem() {
    this.items.push(this.newText)
    this.newText = ''
  }

  removeItem(item: string) {
    let index = this.items.indexOf(item)
    this.items.splice(index, 1)
  }

  handleKeyPress(event: Event) {
    if ((event as KeyboardEvent).key == 'Enter') {
      this.addItem()
    }
  }
}
