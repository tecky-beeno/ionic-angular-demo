import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { map, Observable, timer, interval } from 'rxjs'
import { Product } from '../product'
import { ProductService } from '../product.service'

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {
  id$: Observable<string>
  id: string

  clock = interval(33).pipe(map(counter => new Date()))

  product?: Product

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
  ) {
    this.id$ = route.params.pipe(map(params => params['id']))
    this.id = route.snapshot.params['id']
  }

  async ngOnInit() {
    this.product = await this.productService.getProductDetail(this.id)
  }

  isDiscount(product: Product) {
    return product.price < 50
  }
}
