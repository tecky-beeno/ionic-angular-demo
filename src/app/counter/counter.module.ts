import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { CounterComponent } from './counter.component'
import { IonicModule } from '@ionic/angular'

@NgModule({
  declarations: [CounterComponent],
  imports: [CommonModule, IonicModule],
  exports: [CounterComponent],
})
export class CounterModule {}
