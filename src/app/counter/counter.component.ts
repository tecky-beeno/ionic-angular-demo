import { Component, OnInit } from '@angular/core'
import { CounterService } from '../counter.service'

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
})
export class CounterComponent implements OnInit {
  get counter() {
    return this.counterService.counter
  }

  getCounter() {
    return this.counterService.counter
  }

  constructor(private counterService: CounterService) {}

  ngOnInit() {}

  inc() {
    this.counterService.counter++
  }
}
