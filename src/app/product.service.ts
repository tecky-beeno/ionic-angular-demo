import { Injectable } from '@angular/core'
import { Product } from './product'

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  samples: Product[] = []

  constructor() {
    for (let i = 0; i < 20; i++) {
      this.samples.push({
        id: i + 1,
        name: 'Product ' + (i + 1),
        price: Math.floor(Math.random() * 100 + 10),
        picture: `https://picsum.photos/seed/${i + 1}/200/200`,
      })
    }
  }

  async getProductList() {
    return this.samples
  }

  async getProductDetail(id: number | string) {
    return this.samples.find(product => product.id == +id)
  }
}
