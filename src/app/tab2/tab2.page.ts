import { Component, OnInit } from '@angular/core'
import { Product } from '../product'
import { ProductService } from '../product.service'

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  items: Product[] = []

  constructor(private productService: ProductService) {}

  async ngOnInit() {
    this.items = await this.productService.getProductList()
  }
}
